const unic = (valoare, pozitie, vector) => {
	return vector.indexOf(valoare) === pozitie;
}
//functie luata de pe stackoverflow, la cautarea "how to eliminate duplicates from array javascript"

function distance(first, second) {
	if ( typeof(first) != 'object' || typeof (second) != 'object' ) { //for some reason nu mergea cu isArray()
		throw new Error("InvalidType");
	} else if (first.length === 0 && second.length === 0) return 0;

	firstNoDuplicates = first.filter(unic);
	secondNoDuplicates = second.filter(unic);
	let firstDiff = firstNoDuplicates.filter(function (i) { //creez arrray-ul firstDiff care e FND - SND
		if (secondNoDuplicates.indexOf(i) == -1) {
			return true;
		}
		else {
			return false;
		}
	});
	let secondDiff = secondNoDuplicates.filter(function (i) { //creez array-ul secondDiff care e SND - FND
		if (firstNoDuplicates.indexOf(i) == -1) {
			return true;
		}
		else {
			return false;
		}
	});
	let sauExclusivArray = firstDiff.concat(secondDiff); //creez array-ul prin reuniunea firstDiff cu secondDiff. practic (FND-SND)+(SND-FND) ca la multimi de numere;
	return sauExclusivArray.length;
};

module.exports.distance = distance